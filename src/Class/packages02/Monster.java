package Class.packages02;

public class Monster {
	
//	private String name; // private를 쓰면 Application 에러,
// 해당 클래스 내부에서만 사용가능하다.
	
	private String name;	// 몬스터 이름
	private int hp;			// 몬스터 체력
	
	
	
	public void setname(String name) {
		this.name = name;
	}
	
	
	public void setHp(int hp) {
		
		if(hp > 0) {
			
			System.out.println("체력을 입력한 값으로 변경");
			this.hp = hp;
		} else {
			
			System.out.println("0보다 작거나 같은 값이 입력되어 체력 0으로 변경");
			this.hp = 0;
		}
		
	}
	
	
	public String getInfo() {
		
		return "입력한 " + this.name + "의 체력은 " + this.hp + "입니다.";
	}
	
	
	

}
