package Class.packages02;

public class Application {
	
	public static void main(String[] args) {
		
		
		Monster monster1 = new Monster();	
		monster1.name = "두치";
		monster1.hp = 200;
		System.out.println("============ 1번째 몬스터 ============");
		System.out.println("monster1.name = " + monster1.name);
		System.out.println("monster1.hp = " + monster1.hp);
		
		
		Monster monster2 = new Monster();
		monster2.name = "뿌꾸";
		monster2.hp = -200;
		
		System.out.println("============ 2번째 몬스터 ============");
		System.out.println("monster2.name = " + monster2.name);
		System.out.println("monster2.hp = " + monster2.hp);
		
		
		/* 위에는 [ 직접 접근이라, -200이 되어도 0으로 변하지 않으나, 
		 * 아래쪽으로 보이는것과 같이 메소드를 입력해 매개변수로 값을 가져올 때는 -값을 0 으로 초기화 해준다.
		 * ] */
		
		
		Monster monster3 = new Monster();
		monster3.name = "드라큘라";
		monster3.setHp(200);
		
		System.out.println("============ 3번째 몬스터 ============");
		System.out.println("monster3.name = " + monster3.name);
		System.out.println("monster3.hp = " + monster3.hp);
		
		
		Monster monster4 = new Monster();
		monster4.name = "돼지";
		monster4.setHp(-456456);
		
		System.out.println("============ 4번째 몬스터 ============");
		System.out.println("monster4.name = " + monster4.name);
		System.out.println("monster4.hp = " + monster4.hp);
		
		
		
		/* 이렇게 한뒤에  Monster 에가서 getInfo() 에 다가 name과 hp의 값을 리턴받아 쓸수 있게 해주었다.*/
		
		
		
		
		
		Monster monster5 = new Monster();
		monster5.setname("돼지");
		monster5.setHp(-456456);
		
		System.out.println("============ 5번째 몬스터 ============");
		System.out.println("monster5.name = " + monster5.getInfo());
		
		// 이렇게 getinfo() 를 불러오면 바로 바로 쓸수가 있어 편리하다.
		/* 위에 monster4.name 참조연산자를 통해서 불러오는건, 사용금지,
		 * 
		 * [ 캡슐화 ]를 통해서 private,defeult?,protected, pulbie 등 기본적으로 사용이 원칙이다.
		 * 그래서 Monster 클레스에서 필드에 private를 선언후,
		 * Application에  monster4.name 이런식으로 사용하지말고
		 * 
		 *  Monster 클래스에  set,get을 만들어서 그걸로 불러와야 한다. 
		 *  
		 *  set[값 수정],get[값 가져오기]메소드 
		 * */ 
		
		
		
		
		
	}

}
