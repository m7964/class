package Class.packages01;

public class Field {
	
	
	public static void main(String[] args) {
		
		String id = "user01";
		String pwd = "pass01";
		String name = "홍길동";
		int age = 20;
		char gender = '남';
		String[] hobby = new String[] {"축구","볼링","테니스"};
		
		System.out.println("id : " + id);
		System.out.println("pwd : " + pwd);
		System.out.println("name : " + name);		
		System.out.println("age : " + age);
		System.out.println("gender : " + gender);
		System.out.print("hobby : ");
		for (int i=0; i<hobby.length; i++) {
			System.out.print(hobby[i] + " ");
		}
		System.out.println();
		
		/* 이렇게 각각 변수로 관리하게 되면 단점이 발생
		 * 1. 변수명을 다 관리해야 하는 어려움이 있다.
		 * 2. 모든 회원 정보를 인자로 메소드 호출 시 값을 전달해야 하면 너무 많은 값을들 인자로 전달해야 해서 한 눈에 안들어온다.
		 * 3. 리턴은 1개의 값만 가능하기 때문에 회원정보를 묶어서 리턴값으로 사용 할 수 없다.(서로 다른 자료형이다.)
		 * */
		
		//서로 다른 자료형이다.  ->> 새로운 사용자 정의 타입을 만들었다 CLASS클래스(개발자가 타입을 정의)라고 한다.
		
		// App01Member 이동 
		// 필드선언후 다시 여기로 옴.
		
		/* 사용자 정의 자료형 사용하기
		 * 1. 변수 선언 및 객체 생성
		 * 자료형 변수명 = new 클래스명();  <<< 객체(instance)생성구문,
		 * 
		 * 사용자 정의의 자료형인 클래스를 이용하기 위해선 new 연산자로 이용해서 heap에 공간(할당)하고
		 * 객체를 생성하게 되면 클래스에 객체(instance)가 생성된다 // 기본값 null...등
		 *  */
		
		App01Member member = new App01Member(); // 이제 member 변수로 [ 다른 자료형 ]들을 하나로 관리 가능, heap에 생성
		System.out.println("member.id : " + member.id);
		System.out.println("member.pwd : " + member.pwd);
		System.out.println("member.name : " + member.name);
		System.out.println("member.age : " + member.age);
		System.out.println("member.gender : " + member.gender);
		System.out.println("member.hobby : " + member.hobby);
		/* 필드(App01Member)에 접근하려면  레퍼런스번수명.필드명 으로 써야함.
		 *  '.'은 참조연산자라고 한다 // member 레퍼런스변수라고한다.  필드명은 App01Member에 씀. id.pwd.name등등
		 * */
		
		member.id = "user01";
		member.pwd = "pass01";
		member.name = "홍길동";
		member.age = 20;
		member.gender = '남';
		member.hobby = new String[] {"축구","볼링","테니스"};
		
		System.out.println();
		
		System.out.println("변경 후 member.id : " + member.id);
		System.out.println("변경 후 member.pwd : " + member.pwd);
		System.out.println("변경 후 member.name : " + member.name);
		System.out.println("변경 후 member.age : " + member.age);
		System.out.println("변경 후 member.gender : " + member.gender);
		System.out.print("변경 후 member.hobby : ");
		for(int i=0; i<member.hobby.length; i++) {
			System.out.print(member.hobby[i] + " ");
		}
		
		
		
		
		
		
		
		
		
		
		
	}
	
	
	
	
	
	
	
	
	
	
	

}
