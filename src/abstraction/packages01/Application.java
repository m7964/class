package abstraction.packages01;

import java.util.Scanner;

public class Application {
	
	public static void main(String[] args) {
		
		/* 현실시계의 모든 사건(event)는 객체와 객체의 상호작용에 일어 난다는 세계관을  프로그램을
		 * 만들때 이용하여 새로운 세계를 창조하는 방법론,
		 * - 의인화 기법이 적용되어 현실세계에 불가능한 무생물이나 개념 같은 존재들도 하나의 주제로
		 * 본인의 상태를 스스로 제어하고 행동하도록 한다는 것이 다르다,( 캡슐화의 의미 )
		 * 
		 * */
		
		
		/*  카레이서 자동차 운전 프로그램 만들기  */
		
		/* 1. 자동차는 처음에 멈춘 상태로 대기하고 있는다.
		 * 2. 카레이서는 먼저 자동차에 시동을 건다. 이미 걸려 있는 경우 다시 시동을 걸수 없다.
		 * 3. 카레이서는 엑셀레이터를 밟으면 시동이 걸린 상태일 경우 시속 10km/h씩 증가하며 앞으로 나간다.
		 * 4. 자동차가 달리는 중인 경우 브레이크를 밟으면 자동차의 시속은 0으로 떠러지며 멈춘다.
		 * 5. 브레이크를 밟을때 자동차가 달리는 상태가 아니라면 이미 멈춰 있는 상태라고 한다.
		 * 6. 카레이서가 시동을 끄면 더 이상 자동차는 움직이지 않는다.
		 * 7. 자동차가 달리는 중이라면 시동을 끌수 없다. 
		 *  */
		
		
		/* 필요 한 객체는 : 카레이서 & 자동차 이다.
		 * 
		 * 카레이서가 수신[받으려고]할 메세지 (카레이서가 해야할 행위)
		 * 1. 카레이서는 시동을 건다.
		 * 2. 엑셀레이터를 밟아라.
		 * 3. 브레이크를 밟아라 
		 * 4. 시동을 꺼라
		 * 
		 * 
		 * 자동차가 수신[받으려고]할 수 있는 메세지 ( 자동차가 해야할 행위 )
		 * 1. 시동을 걸어라
		 * 2. 앞으로 가라
		 * 3. 멈춰라
		 * 4. 시동을 꺼라
		 * */
		
		
		/* Car,CarRacer 하고 여기로 온다. */
		
		CarRacer racer = new CarRacer();
		Scanner sc = new Scanner(System.in);
		
		while(true) {
			System.out.println("====== 카레이싱 프로그램 ======");
			System.out.println("1. 시동 걸기 ");
			System.out.println("2. 전진");
			System.out.println("3. 정지");
			System.out.println("4. 시동 끄기");
			System.out.println("9. 프로그램 종로");
			System.out.print("메뉴 선택 : ");
			int no = sc.nextInt();
			
			
			switch(no) {
				case 1 : racer.startUp(); break;
				case 2 : racer.stepAccelator(); break;
				case 3 : racer.stepBreak(); break;
				case 4 : racer.turnOff(); break;
				case 9 : System.out.println("프로그램을 종료합니다. "); break;
				default : System.out.println("잘못된 번호"); break;
			}
			
			if(no == 9) {
				break;
			}
			
		}
		sc.close();
		
		
		
		
		
		
		
	}

}
























