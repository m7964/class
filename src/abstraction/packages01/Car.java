package abstraction.packages01;

public class Car {
	
	private boolean isOn; // 시동이 꺼진 false상태
	private int speed;    // 속도
	
	
	
	/**
	 * <pre>
	 * 시동을 걸기 위한 메소드
	 * </pre>
	 */
	public void startUp() {
		
		if(isOn) {
			System.out.println("이미 시동이 걸려 있습니다.");
		} else {
			this.isOn = true;
			System.out.println("시동을 걸었습니다. ");
		}
	}

	
	/**
	 * <pre>
	 *  자동차 가속시키기 위한 메소드 
	 * </pre>
	 * 
	 */
	public void go() {
		
		if(isOn) {
			System.out.println("차가 앞으로 움직인다.");
			this.speed += 10;
			System.out.println("현재 차의 시속은 " + this.speed + "km/h 입니다.");
		} else {
			System.out.println("시동을 켜주세요.");
		}
	}
	
	
	
	/**
	 * <pre>
	 *  자동차를 멈추기 위한 메소드 
	 * </pre>
	 * 
	 */
	public void stop() {
		if(isOn) {
			if(this.speed > 0) { // 달리고 있는 상태인 경우
				this.speed = 0;
				System.out.println("브레이크 차멈춥니다.");
			}else {
				System.out.println("차는 이미 멈춰 있는 상태입니다. ");
			}
		}  else {
			System.out.println("차에 시동이 걸려 있지 않습니다.");
			
		}
	}
	
	/**
	 * <pre>
	 *  차에 시동을 끄는 메소드 
	 * </pre>
	 */
	public void turnOff() {
		
		if(isOn) {
			if(speed>0) {
				System.out.println("차를 우선 멈춰주세요.");
			} else {
				this.isOn = false;
				System.out.println("시동을 끕니다.");
			}
		} else {
			System.out.println("이미 시동이 꺼져있습니다.");
		}
		
		
	}
	
	
	
	
	
	
	

}






























