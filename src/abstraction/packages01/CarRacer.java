package abstraction.packages01;

public class CarRacer {
	
	private Car car = new Car();
	
	/**
	 * <pre>
	 *  카레이서가 자동차의 시동을 걸 수있는 메소드
	 * </pre>
	 * 
	 */
	public void startUp() {
		car.startUp();
		
	}
	
	/**
	 * <pre>
	 *  카레이서가 엑셀레이터를 밟아 가속을 할 수 있는 메소드 
	 * </pre>
	 * 
	 */
	public void stepAccelator() {
		car.go();
	}
	
	
	public void stepBreak()  {
		car.stop();
	}
	
	public void turnOff() {
		car.turnOff();
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
}
