package constructor.packages01;

public class Application {
	
	public static void main(String[] args) {
		
		
		/* 클래스명 레퍼런스 변수 = new 클래스명(); 앞에서 했던 이거는
		 * 클래스명 레퍼런스변수 = new 생성자(); 표현이 맞는 표현이다.
		 * */
		
		/* 생성자란 ? 
		 * : 인스턴스(객체)를 생성할 때 초기 수행할 명령이 있는 경우 미리 작성해두고, 인스턴스를 생성할때 호출 된다. 
		 * 생성자 함수에 매개변수가 없는 생성자를 [ 기본생성자 ] 라고 하며, 
		 * 
		 * 인자를 받아 필드를  초기화할 목적의 생성자를 [ 매개변수 있는 생성자 ] 라고 한다.
		 * 
		 * --> 매개변수 있는 생성자가 1개라도 존재하는 경우 기본생성자를 기본으로 추가해주지 않는다.
		 * 우리가 직접 만들어서 넣어줘야한다. 
		 **/
		
		User user1 = new User();
		System.out.println(user1.getInfo());
		
		User user2 = new User("user01","pass01","홍길동");
		System.out.println(user2.getInfo());
		
		User user3 = new User("user02","pass02","이순신",new java.util.Date());
		System.out.println(user3.getInfo());
				
	}

}


























