package constructor.packages01;

public class User {
	
	private String id;
	private String pwd;
	private String name;
	private java.util.Date enrollDate; // 필드로 다른 클래스 자료형도 사용 할수 있다.(String도 그런거였다)
	
	
	/* 생성자의 사용목적 
	 * 1. 인스턴스 생성 시점에 수행할 명령이 있는 경우 사용한다.
	 * 2. 매개변수 있는 생성자의 경우 매개변수로 전달받은 값으로 필드를 초기화하여 인스턴스를 생성할 목적으로 주로 사용한다.
	 * 3. 작성한 생성자 외에는 인스턴스를 생성하는 방법을 제공하지 않는다는 의미를 가진다.
	 * 따라서, 인스턴스를 생성할 방법을 제한하기 위한 용도로 사용할 수도 있다. */
	
	/* 생성자 작성 방법
	 * [ 표현식 ]
	 * 접근제한자 클래스멍([매개변수]) {
	 * 		인스턴스 생성 시점에 수행할 명령 기술(주로 필드를 초기화)
	 * 		this.필드명 = 매개변수; // 설정자(setter) 여러개의 기능을 한번에 호출로 수행할 수 있따.
	 * }
	 * */
	
	public User() {		// 기본생성자
		System.out.println("User클래스의 기본생성자");
	}
	
	public User(String id, String pwd, String name) {
		this.id = id;
		this.pwd = pwd;
		this.name = name;
		System.out.println("User 클래스의 id,pwd,name을 초기화하는 생성자 호출");
	}
	
	public User(String id, String pwd, String name, java.util.Date enrollDate) {
		this.id = id;
		this.pwd = pwd;
		this.name = name;
		this.enrollDate = enrollDate;
		System.out.println("User 클래스의 모든 필드 초기화 하는 생성자 호출함..");
		
	}
	
	
	
	/**
	 * <pre>
	 *  문자열 합치기
	 * </pre>
	 * @return
	 */
	public String getInfo() {
		
		return "id = " + this.id + ", pwd = " + this.pwd + ", name = " + this.name + ", enorllDate = " + this.enrollDate;
	}
	
	
	
	
	
	
}



















