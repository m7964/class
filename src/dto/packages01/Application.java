package dto.packages01;

public class Application {
	
	public static void main(String[] args) {
		
		/* 캡슐화의 원칙에는 이룹 어긋나긴 하지만 매번 추상화를 하지 않아도 되는 객체도 존재한다.
		 * 행위 위주가 아닌 데이터를 하나로 뭉치기 위한 객체(Data Transfer Object)의 경우 / DTO
		 * */
		
		
		
		MemberDTO member = new MemberDTO();
		member.setNumber(1);
		member.setName("홍길동");
		member.setAge(20);
		member.setGender('남');
		member.setHeight(180.5);
		member.setWeight(70.5);
		member.setActivated(true);
		
		
		System.out.println("회원번호 : " + member.getNumber());
		System.out.println("회원명 : " + member.getName());
		System.out.println("회원나이 : " + member.getAge());
		System.out.println("회원성별 : " + member.getGender());
		System.out.println("회원키 : " + member.getHeight());
		System.out.println("회원몸무게 : " + member.getWeight());
		System.out.println("회원활성화여부 : " + member.isActivated());
		
	}

}
